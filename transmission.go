// transmission_exporter is a Node exporter textfile runner script that
// outputs Transmission stats as Prometheus metrics.
package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	fileName = "stats.json"
)

// Stats struct contains all the stats collected by Transmission.
type Stats struct {
	DownloadedBytes uint64 `json:"downloaded-bytes"`
	FilesAdded      uint64 `json:"files-added"`
	SecondsActive   uint64 `json:"seconds-active"`
	SessionCount    uint64 `json:"session-count"`
	UploadedBytes   uint64 `json:"uploaded-bytes"`
}

var downloadedBytes = prometheus.NewGauge(
	prometheus.GaugeOpts{
		Name: "transmission_downloaded_bytes_total",
		Help: "Total number of bytes downloaded",
	},
)

var filesAdded = prometheus.NewGauge(
	prometheus.GaugeOpts{
		Name: "transmission_added_files_total",
		Help: "Total number of files added",
	},
)

var secondsActive = prometheus.NewGauge(
	prometheus.GaugeOpts{
		Name: "transmission_running_seconds_total",
		Help: "Total number of seconds Transmission ran for across all sessions",
	},
)

var sessionCount = prometheus.NewGauge(
	prometheus.GaugeOpts{
		Name: "transmission_sessions_total",
		Help: "Total number of times Transmission was started",
	},
)

var uploadedBytes = prometheus.NewGauge(
	prometheus.GaugeOpts{
		Name: "transmission_uploaded_bytes_total",
		Help: "Total number of bytes uploaded",
	},
)

func (t Stats) String() string {
	bytes, err := json.Marshal(t)
	if err != nil {
		log.Fatal(err)
	}
	return string(bytes)
}

func init() {
	prometheus.MustRegister(downloadedBytes)
	prometheus.MustRegister(filesAdded)
	prometheus.MustRegister(secondsActive)
	prometheus.MustRegister(sessionCount)
	prometheus.MustRegister(uploadedBytes)
}

func main() {
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}

	var stats Stats
	json.Unmarshal(bytes, &stats)

	downloadedBytes.Set(float64(stats.DownloadedBytes))
	filesAdded.Set(float64(stats.FilesAdded))
	secondsActive.Set(float64(stats.SecondsActive))
	sessionCount.Set(float64(stats.SessionCount))
	uploadedBytes.Set(float64(stats.UploadedBytes))

	http.Handle("/metrics", promhttp.Handler())
	panic(http.ListenAndServe(":8080", nil))
}
