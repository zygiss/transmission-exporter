# Transmission exporter

This exporter is written as a test and isn't intended to be used in
production.  If you insist however, then do with it as you please.

If you want something more serious, give [this
exporter](https://github.com/metalmatze/transmission-exporter/) a try
instead.
